declare abstract class Nanocomponent {
  constructor(name?: string)
  render(...arguments: any[]): HTMLElement
  readonly element: HTMLElement
  abstract createElement(...arguments: any[]): HTMLElement
  abstract update(...arguments: any[]): boolean
  beforerender(el: HTMLElement): void
  load(el: HTMLElement): void
  unload(el: HTMLElement): void
  afterupdate(el: HTMLElement): void
  afterreorder(el: HTMLElement): void
  rerender(): void
  private _arguments: Object
}

export = Nanocomponent
